/* 
 * Copyright 2014 Florian Richter.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.floric.tablejumping.org.validator;

import de.floric.tablejumping.org.base.Pair;
import de.floric.tablejumping.org.base.Person;
import java.util.ArrayList;

/**
 *
 * @author florian
 */
public interface Verifiable {

    String getDescription();
    String getErrorMessage();
    void setInput(ArrayList<Pair> pairs, ArrayList<Person> persons);
    void doCheck();
    Boolean isErrorFree();
    
}
