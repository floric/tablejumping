/* 
 * Copyright 2014 Florian Richter.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.floric.tablejumping.org.validator;

import de.floric.tablejumping.org.base.Address;
import de.floric.tablejumping.org.base.Person;
import de.floric.tablejumping.util.EmailParser;
import java.util.ArrayList;

/**
 *
 * @author florian
 */
public class PersonDetailCheck implements Verifiable {

    private boolean isValid = false;
    private ArrayList<Person> persons = new ArrayList<Person>();
    private String errorMessage = "";

    public PersonDetailCheck() {
        setPersons(persons);
    }

    public void setPersons(ArrayList<Person> persons) {
        if (persons != null) {
            this.persons = persons;
        }
    }

    @Override
    public String getDescription() {
        return "Dieser Test stellt sicher, dass allen Personen alle gültigen Informationen zugeordnet sind.";
    }

    @Override
    public String getErrorMessage() {
        return errorMessage;
    }

    @Override
    public Boolean isErrorFree() {
        return isValid;
    }

    @Override
    public void doCheck() {
        isValid = true;

        StringBuilder strBuild = new StringBuilder();
        
        for (Person p : persons) {
            if (p.getPreName().compareTo("") == 0) {
                isValid = false;
                strBuild.append("Eine Person hat einen leeren Vornamen!\n");
            }
            if (p.getLastName().compareTo("") == 0) {
                isValid = false;
                strBuild.append("Eine Person hat einen leeren Nachnamen!\n");
            }

            String mail = p.getMail();
            EmailParser mailParser = new EmailParser();

            if (mailParser.validate(mail) == false) {
                isValid = false;
                strBuild.append("Die Mailadresse von ").append(p.getPreName()).append(" ").append(p.getLastName()).append(" ist nicht gültig!\n");
            }

            try {
                Integer.parseInt(p.getTelephone());
            } catch (NumberFormatException e) {
                isValid = false;
                strBuild.append("Die Telefonnummer von ").append(p.getPreName()).append(" ").append(p.getLastName()).append(" ist nicht gültig!\n");
            };

            Address adr = p.getAddress();
            if (adr.getStreet().compareTo("") == 0) {
                isValid = false;
                strBuild.append("Die Stadt von ").append(p.getPreName()).append(" ").append(p.getLastName()).append(" ist nicht gültig!\n");
            }
            if (adr.getCity().compareTo("") == 0) {
                isValid = false;
                strBuild.append("Die Stadt von ").append(p.getPreName()).append(" ").append(p.getLastName()).append(" ist nicht gültig!\n");
            }
            if (adr.getLatitude() == 0 && adr.getLongitute() == 0) {
                isValid = false;
                strBuild.append("Die Ortsberechnung von  ").append(p.getPreName()).append(" ").append(p.getLastName()).append(" ist nicht gültig!\n");
                
            }
            try {
                if (adr.getZipCode().length() != 5) {
                    throw new NumberFormatException();
                }

                Integer.parseInt(adr.getZipCode());
            } catch (NumberFormatException e) {
                isValid = false;
                strBuild.append("Die Postleitzahl von ").append(p.getPreName()).append(" ").append(p.getLastName()).append(" ist nicht gültig!\n");
            };

            /*if (p.getIsPartOf() == null) {
             isValid = false;
             strBuild.append(p.getPreName()).append(" ").append(p.getLastName()).append(" ist in keinem Paar zugeordnet!\n");
             }*/
        }

        errorMessage = strBuild.toString();
    }

    @Override
    public String toString() {
        return "Personendetails";

    }

    @Override
    public void setInput(ArrayList pairs, ArrayList persons) {
        this.persons = persons;
    }

}
