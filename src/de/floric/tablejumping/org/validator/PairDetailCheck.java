/* 
 * Copyright 2014 Florian Richter.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.floric.tablejumping.org.validator;

import de.floric.tablejumping.org.base.Address;
import de.floric.tablejumping.org.base.Pair;
import java.util.ArrayList;

/**
 *
 * @author florian
 */
public class PairDetailCheck implements Verifiable {

    private boolean isValid = false;
    private ArrayList<Pair> pairs;
    private String errorMessage = "";

    @Override
    public String getDescription() {
        return "Dieser Test stellt sicher, dass allen Paaren alle gültigen Informationen zugeordnet sind.";
    }

    public void setPairs(ArrayList<Pair> pairs) {
        this.pairs = pairs;
    }

    @Override
    public Boolean isErrorFree() {
        return isValid;
    }

    @Override
    public void doCheck() {
        isValid = true;

        StringBuilder strBuild = new StringBuilder();

        for (Pair p : pairs) {
            if (p.getpOne() == null) {
                strBuild.append(("Person 1 des Paares " + p.getID() + " ist nicht gültig!\n"));
                isValid = false;
            }
            if (p.getpTwo() == null) {
                strBuild.append(("Person 2 des Paares " + p.getID() + " ist nicht gültig!\n"));
                isValid = false;
            }
            if (p.getpOne().equals(p.getpTwo())) {
                strBuild.append(("Person 1 und 2 des Paares " + p.getID() + " sind identisch!\n"));
                isValid = false;
            }
            
            Address hostAddr = p.getHostAddress();
            if (hostAddr == null) {
                strBuild.append("Paar " + p.getID() + " kann keine Küche stellen!\n");
                isValid = false;
            }
        }
        errorMessage = strBuild.toString();

    }

    @Override
    public String getErrorMessage() {
        return errorMessage;
    }

    @Override
    public String toString() {
        return "Paardetails";
    }

    @Override
    public void setInput(ArrayList pairs, ArrayList persons) {
        this.pairs = pairs;
    }

}
