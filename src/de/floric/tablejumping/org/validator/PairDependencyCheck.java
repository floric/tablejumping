/* 
 * Copyright 2014 Florian Richter.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.floric.tablejumping.org.validator;

import de.floric.tablejumping.org.base.Pair;
import de.floric.tablejumping.org.base.Person;
import java.util.ArrayList;

/**
 *
 * @author florian
 */
public class PairDependencyCheck implements Verifiable {

    private boolean isValid = false;
    private ArrayList<Pair> pairs;
    private ArrayList<Person> persons;
    private String errorMessage = "";

    @Override
    public String getDescription() {
        return "Dieser Test stellt sicher, dass zu jedem Paar genau zwei eindeutige Personen gehören.";
    }

    public void setPairs(ArrayList<Pair> pairs) {
        this.pairs = pairs;
    }

    @Override
    public Boolean isErrorFree() {
        return isValid;
    }

    @Override
    public void doCheck() {
        isValid = true;

        StringBuilder strBuild = new StringBuilder();

        ArrayList<Person> personCpy = new ArrayList<>(persons);

        if((pairs.size() % 3) != 0) { // check for the right numer of pairs
            strBuild.append(("Die Anzahl der Paare muss ein Vielfaches von 3 sein, damit die Verteilung aufgeht!\n"));
            isValid = false;
        }
        
        if (checkForDoublePairs() == false) { // check for double pairs
            strBuild.append("Es sind Paare doppelt vertreten!\n");
            isValid = false;
        }

        for (Pair p : pairs) { // check persons
            checkPersonInList(p.getpOne(), p, personCpy, strBuild);
            checkPersonInList(p.getpTwo(), p, personCpy, strBuild);
        }

        if (!(personCpy.isEmpty())) {
            for (Person p : personCpy) {
                strBuild.append((p.getPreName() + " " + p.getLastName() + " ist keinem Paar zugeordnet!\n"));
            }
            isValid = false;
        }

        errorMessage = strBuild.toString();

    }

    private void checkPersonInList(Person p, Pair pair, ArrayList<Person> personList, StringBuilder strBuild) {
        if (personList.contains(p)) {
            personList.remove(p);
        } else {
            strBuild.append((p.getPreName() + " " + p.getLastName() + " ist nicht in genau einem Paar enthalten!\n"));
            isValid = false;
        }
    }

    private boolean checkForDoublePairs() {
        boolean checkPassed = true;

        for (Pair p1 : pairs) {
            int equalObjs = 0;

            for (Pair p2 : pairs) {
                if (p1.equals(p2)) {
                    equalObjs++;
                }
            }
            if (equalObjs != 1) {
                checkPassed = false;
            }

        }

        return checkPassed;
    }

    @Override
    public String getErrorMessage() {
        return errorMessage;
    }

    @Override
    public String toString() {
        return "Paarabhängigkeiten";
    }

    @Override
    public void setInput(ArrayList pairs, ArrayList persons) {
        this.pairs = pairs;
        this.persons = persons;
    }

}
