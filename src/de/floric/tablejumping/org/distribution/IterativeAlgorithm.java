/*
 * Copyright 2014 florian.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.floric.tablejumping.org.distribution;

import de.floric.tablejumping.org.base.Dessert;
import de.floric.tablejumping.org.base.MainCourse;
import de.floric.tablejumping.org.base.Meal;
import de.floric.tablejumping.org.base.Meeting;
import de.floric.tablejumping.org.base.Pair;
import de.floric.tablejumping.org.base.Person;
import de.floric.tablejumping.org.base.Starter;
import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author florian
 */
public class IterativeAlgorithm extends CalcAlgorithm {

    private final ArrayList<Meeting> starterMeetings;
    private final ArrayList<Meeting> mainCourseMeetings;
    private final ArrayList<Meeting> dessertMeetings;

    private final Random rnd;

    private static final long SEED = 1L;

    public IterativeAlgorithm(ArrayList<Pair> pairs, ArrayList<Person> persons) {
        super(pairs, persons);

        starterMeetings = new ArrayList<>();
        mainCourseMeetings = new ArrayList<>();
        dessertMeetings = new ArrayList<>();
        meetings = new ArrayList<>();
        rnd = new Random(SEED);
    }

    @Override
    public void doCalculation() {
        meetings.clear();
        starterMeetings.clear();
        mainCourseMeetings.clear();
        dessertMeetings.clear();

        System.out.println("Started");

        initDistribution();

        System.out.println("Distance to travel:" + getTotalDistanceSum());
    }

    private void initDistribution() {
        ArrayList<Pair> pairPot = new ArrayList<>();

        pairPot.addAll(pairs);

        int nrOfStarters = 0;
        int norOfMainCourses = 0;
        int nrOfDesserts = 0;

        createMeetings(pairPot, new Starter());
        createMeetings(pairPot, new MainCourse());
        createMeetings(pairPot, new Dessert());

        System.out.println("Meetings created!");

        pairPot.addAll(pairs);

        distributePairsToMeetings(starterMeetings);
        distributePairsToMeetings(mainCourseMeetings);
        distributePairsToMeetings(dessertMeetings);

        meetings.addAll(starterMeetings);
        meetings.addAll(mainCourseMeetings);
        meetings.addAll(dessertMeetings);

    }

    private void createMeetings(ArrayList<Pair> pairPot, Meal meal) {

        for (int i = 0; i < pairs.size() / 3; i++) {
            Pair p = pairPot.get(rnd.nextInt(pairPot.size()));

            Meeting uniqueMeeting = new Meeting(meal, p, p.getHostAddress());

            if (meal instanceof Starter) {
                p.setStarter(uniqueMeeting);
                starterMeetings.add(uniqueMeeting);
            } else if (meal instanceof MainCourse) {
                p.setMainCourse(uniqueMeeting);
                mainCourseMeetings.add(uniqueMeeting);
            } else if (meal instanceof Dessert) {
                p.setDessert(uniqueMeeting);
                dessertMeetings.add(uniqueMeeting);
            }

            pairPot.remove(p);

        }
    }

    private void distributePairsToMeetings(ArrayList<Meeting> meetingType) {

        ArrayList<Pair> pairPot = new ArrayList<>();
        ArrayList<Meeting> meetingPot = new ArrayList<>();

        pairPot.addAll(pairs);
        meetingPot.addAll(meetingType);

        // remove all pairs which are already hosts of this course
        for (Meeting m : meetingPot) {
            pairPot.remove(m.getHost());
        }

        do {
            int nextPairIndex = rnd.nextInt(pairPot.size());

            Pair p = pairPot.get(nextPairIndex);

            Meeting m = meetingPot.get(rnd.nextInt(meetingPot.size()));

            // if guest slots are free add a guest
            if (m.getGuestOne() == null || m.getGuestTwo() == null) {

                if (m.getMeal() instanceof Starter) {
                    p.setStarter(m);
                } else if (m.getMeal() instanceof MainCourse) {
                    p.setMainCourse(m);
                } else if (m.getMeal() instanceof Dessert) {
                    p.setDessert(m);
                }

                m.addGuest(p);
                pairPot.remove(p);

            } else {
                meetingPot.remove(m);
            }

        } while (!(pairPot.isEmpty()));
    }

}
