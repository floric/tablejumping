/* 
 * Copyright 2014 Florian Richter.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.floric.tablejumping.org.distribution;

import de.floric.tablejumping.org.base.Meeting;
import de.floric.tablejumping.org.base.Pair;
import de.floric.tablejumping.org.base.Person;
import java.util.ArrayList;

/**
 *
 * @author florian
 */
abstract public class CalcAlgorithm {
    
    protected ArrayList<Pair> pairs = new ArrayList<>();
    protected ArrayList<Person> persons = new ArrayList<>();
    protected ArrayList<Meeting> meetings = new ArrayList<>();
    protected double minimumTravelDistance = 0.0;
    protected double differentPartners = 0.0;

    public CalcAlgorithm(ArrayList<Pair> pairs, ArrayList<Person> persons) {
        this.pairs = pairs;
        this.persons = persons;
    }

    public String getDescription() {
        return "Calc algorithm";
    }

    public void setImportanceValues(double minimumTravelDistance, double differentPartners) {
        this.minimumTravelDistance = minimumTravelDistance;
        this.differentPartners = differentPartners;
    }

    public void setInput(ArrayList<Pair> pairs, ArrayList<Person> persons) {
        this.pairs = pairs;
        this.persons = persons;
    }

    abstract public void doCalculation();

    public ArrayList<Meeting> getResults() {
        return meetings;
    }
    
    protected double getTotalDistanceSum() {
        double distanceSum = 0;
        
        for(Pair p: pairs) {
            distanceSum += p.getDistanceSum();
        }
        
        return distanceSum;
    }

}
