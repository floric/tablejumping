/* 
 * Copyright 2014 Florian Richter.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.floric.tablejumping.org.distribution;

import de.floric.tablejumping.org.base.Dessert;
import de.floric.tablejumping.org.base.MainCourse;
import de.floric.tablejumping.org.base.Meeting;
import de.floric.tablejumping.org.base.Pair;
import de.floric.tablejumping.org.base.Person;
import de.floric.tablejumping.org.base.Starter;
import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author florian
 */
public class EvolutionaryAlgorithm extends CalcAlgorithm {

    private final ArrayList<Meeting> starterMeetings;
    private final ArrayList<Meeting> mainCourseMeetings;
    private final ArrayList<Meeting> dessertMeetings;

    private final Random rnd;

    private static final long SEED = 1L;
    private static final int FAILED_ITERATIONS = 10000;

    public EvolutionaryAlgorithm(ArrayList<Pair> pairs, ArrayList<Person> persons) {
        super(pairs, persons);

        starterMeetings = new ArrayList<>();
        mainCourseMeetings = new ArrayList<>();
        dessertMeetings = new ArrayList<>();
        meetings = new ArrayList<>();
        rnd = new Random(SEED);
    }

    @Override
    public void doCalculation() {
        meetings.clear();
        starterMeetings.clear();
        mainCourseMeetings.clear();
        dessertMeetings.clear();

        initDistribution();

        System.out.println("Initial distance to travel:" + getTotalDistanceSum());

        int nrOfFailedChecks = 0;
        
        while(nrOfFailedChecks < FAILED_ITERATIONS) {
            if(checkForImprovement(starterMeetings) == false) {
                nrOfFailedChecks++;
            } else {
                nrOfFailedChecks = 0;
            }
            if(checkForImprovement(mainCourseMeetings) == false) {
                nrOfFailedChecks++;
            } else {
                nrOfFailedChecks = 0;
            }
            if(checkForImprovement(dessertMeetings) == false) {
                nrOfFailedChecks++;
            } else {
                nrOfFailedChecks = 0;
            }
        }
        
        System.out.println("Minimal distance after " + FAILED_ITERATIONS + " failed optimization tries:" + getTotalDistanceSum());

    }

    private boolean checkForImprovement(ArrayList<Meeting> list) {
        if (list != null) {
            int comparedItem = 0;
            int selectedItem = rnd.nextInt(list.size());
            
            do {
                comparedItem = rnd.nextInt(list.size());
            } while(comparedItem == selectedItem);
            
            double oldDistanceSum = getTotalDistanceSum();

            Meeting meetingA = list.get(selectedItem);
            Meeting meetingB = list.get(comparedItem);
            
            int guestA = rnd.nextInt(2);
            int guestB = rnd.nextInt(2);
            
            swapGuests(meetingA, meetingB, guestA, guestB);

            double newDistanceSum = getTotalDistanceSum();

            if (oldDistanceSum <= newDistanceSum) {
               swapGuests(meetingA, meetingB, guestA, guestB);
               return false;
            } else {
               return true;
            }
        } else {
            return false;
        }
    }
    
    private static void swapGuests(Meeting meetingA, Meeting meetingB, int guestA, int guestB) {
        Pair temp;

        if(guestA == 1) {
            temp = meetingA.getGuestOne();
        } else {
            temp = meetingA.getGuestTwo();
        }
        
        if(guestB == 1) {
            if(guestA == 1) {
                meetingA.setGuestOne(meetingB.getGuestOne());
                meetingB.setGuestOne(temp);
            } else {
                meetingA.setGuestTwo(meetingB.getGuestOne());
                meetingB.setGuestOne(temp);
            }
        } else {
            if(guestA == 1) {
                meetingA.setGuestOne(meetingB.getGuestTwo());
                meetingB.setGuestTwo(temp);
            } else {
                meetingA.setGuestTwo(meetingB.getGuestTwo());
                meetingB.setGuestTwo(temp);
            }
        }
    }

    private void initDistribution() {

        // starter
        for (int i = 0; i < pairs.size(); i += 3) {
            Pair host = pairs.get(i);
            Pair guestOne = pairs.get(i + 1);
            Pair guestTwo = pairs.get(i + 2);

            Starter starter = new Starter();

            Meeting uniqueMeeting = new Meeting(starter, host, guestOne, guestTwo, host.getHostAddress());

            host.setStarter(uniqueMeeting);
            guestOne.setStarter(uniqueMeeting);
            guestTwo.setStarter(uniqueMeeting);

            starterMeetings.add(uniqueMeeting);
        }

        // main course
        for (int i = 0; i < pairs.size(); i += 3) {
            Pair host = pairs.get(i + 1);
            Pair guestOne = pairs.get(i);
            Pair guestTwo = pairs.get(i + 2);

            MainCourse mainCourse = new MainCourse();

            Meeting uniqueMeeting = new Meeting(mainCourse, host, guestOne, guestTwo, host.getHostAddress());

            host.setMainCourse(uniqueMeeting);
            guestOne.setMainCourse(uniqueMeeting);
            guestTwo.setMainCourse(uniqueMeeting);

            mainCourseMeetings.add(uniqueMeeting);
        }

        // dessert
        for (int i = 0; i < pairs.size(); i += 3) {
            Pair host = pairs.get(i + 2);
            Pair guestOne = pairs.get(i);
            Pair guestTwo = pairs.get(i + 1);

            Dessert dessert = new Dessert();

            Meeting uniqueMeeting = new Meeting(dessert, host, guestOne, guestTwo, host.getHostAddress());

            host.setDessert(uniqueMeeting);
            guestOne.setDessert(uniqueMeeting);
            guestTwo.setDessert(uniqueMeeting);

            dessertMeetings.add(uniqueMeeting);
        }

        meetings.addAll(starterMeetings);
        meetings.addAll(mainCourseMeetings);
        meetings.addAll(dessertMeetings);
    }

}
