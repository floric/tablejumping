/* 
 * Copyright 2014 Florian Richter.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.floric.tablejumping.org.base;

import java.util.Date;

/**
 *
 * @author Florian Richter
 */
public class Meeting {

    private final int ID;
    private static int startId = 0;
    
    private Pair host;
    private Pair guestOne;
    private Pair guestTwo;
    private Address place;
    private Date time;
    private Meal meal;
    
    public Meeting(Meal meal, Pair host, Pair guestOne, Pair guestTwo, Address place) {
        this.host = host;
        this.guestOne = guestOne;
        this.guestTwo = guestTwo;
        this.place = place;
        this.meal = meal;
        this.ID = getNextId();
    }
    
    public Meeting(Meal meal, Pair host, Address place) {
        this.meal = meal;
        this.host = host;
        this.place = place;
        this.meal = meal;
        this.ID = getNextId();
    }

    public Pair getHost() {
        return host;
    }

    public void setHost(Pair host) {
        this.host = host;
    }

    public Pair getGuestOne() {
        return guestOne;
    }

    public void setGuestOne(Pair guestOne) {
        if(this.getMeal() instanceof Starter) {
            guestOne.setStarter(this);
        } else if(this.getMeal() instanceof MainCourse) {
            guestOne.setMainCourse(this);
        } else if(this.getMeal() instanceof Dessert) {
            guestOne.setDessert(this);
        }
       
        this.guestOne = guestOne;
    }

    public Pair getGuestTwo() {
        return guestTwo;
    }

    public void setGuestTwo(Pair guestTwo) {
        if(this.getMeal() instanceof Starter) {
            guestTwo.setStarter(this);
        } else if(this.getMeal() instanceof MainCourse) {
            guestTwo.setMainCourse(this);
        } else if(this.getMeal() instanceof Dessert) {
            guestTwo.setDessert(this);
        }
        
        this.guestTwo = guestTwo;
    }
    
    public void addGuest(Pair guest) {
        if(guestOne == null) {
            setGuestOne(guest);
        } else if(guestTwo == null) {
            setGuestTwo(guest);
        }
    }

    public Address getPlace() {
        return place;
    }

    public void setPlace(Address place) {
        this.place = place;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Meal getMeal() {
        return meal;
    }

    public void setMeal(Meal meal) {
        this.meal = meal;
    }

    @Override
    public String toString() {
        return "Meeting: Host(" + host.getID() + "), Guest1(" + guestOne + "), Guest2(" + guestTwo + ") at (" + place + ") eating: " + meal;
    }
    
    private int getNextId() {
        startId++;
        return startId;
    }
    
}
