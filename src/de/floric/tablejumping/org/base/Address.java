/* 
 * Copyright 2014 Florian Richter.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.floric.tablejumping.org.base;

import de.floric.tablejumping.util.Geocoding;

/**
 *
 * @author florian
 */
public class Address {
    
    private String street;
    private String city;
    private String zipCode;
    private String notice;
    private double latitude;
    private double longitute;

    public Address(String street, String city, String zipCode) {
        this.street = street;
        this.city = city;
        this.zipCode = zipCode;
        this.notice = "";
        this.latitude = 0;
        this.longitute = 0;
        
        if(street.compareTo("") != 0 && city.compareTo("") != 0) {
            this.calcCoords();
        }
    }
    
    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getNotice() {
        return notice;
    }

    public void setNotice(String notice) {
        this.notice = notice;
    }
    
    public double getDistanceTo(Address anotherAddr) {
        
        return 10;
    }

    public double getLongitute() {
        return longitute;
    }

    public void setLongitute(double longitute) {
        this.longitute = longitute;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }
    
    public void calcCoords() {
        Geocoding geoCoding = new Geocoding(this);

        if (geoCoding.fetchCoordinates()) {
            this.setLatitude(geoCoding.getLatitude());
            this.setLongitute(geoCoding.getLongitute());

            System.out.println(this.toString());
        } else {
            System.out.println("Error at fetching the coordinates for " + this.getStreet() + ", " + this.getCity() + "!");
            this.setLatitude(0);
            this.setLongitute(0);
        }
    }
    
    @Override
    public String toString() {
        return getStreet() + ", " + getCity() + ", " + getZipCode() + "; Coords: " + getLatitude() + ", " + getLongitute() + "; note: " + notice;
    }
    
}
