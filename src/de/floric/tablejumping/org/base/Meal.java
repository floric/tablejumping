/* 
 * Copyright 2014 Florian Richter.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.floric.tablejumping.org.base;

/**
 *
 * @author florian
 */
public class Meal {
    
    private String title;
    private String notice;
    private int duration = 0;
    
    Meal(String title, int duration) {
        this.title = title;
        this.duration = duration;
    }
    
    Meal() {
        this.title = "unbenannt";
        this.notice = "";
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNotice() {
        return notice;
    }

    public void setNotice(String notice) {
        this.notice = notice;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }
    
    @Override
    public String toString() {
        return this.getClass() + ": " + title + "; time: " + duration;
    }
    
}
