/* 
 * Copyright 2014 Florian Richter.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.floric.tablejumping.org.base;

import java.util.ArrayList;

/**
 *
 * @author florian
 */
public class Person implements Comparable<Person> {

    private final int ID;
    private static int startId = 0;
    
    private String preName;
    private String lastName;
    private String telephone;
    private String mail;
    private Address address;
    private String notice;
    private boolean hasKitchen;
    private Pair isPartOf;

    public Person(String preName, String lastName, String telephone, String mail, Address address, String notice, boolean hasKitchen) throws InstantiationException {
        if (preName != null && lastName != null && telephone != null && mail != null && address != null && notice != null) {
            setPreName(preName);
            setLastName(lastName);
            setTelephone(telephone);
            setMail(mail);
            setAddress(address);
            setNotice(notice);
            setHasKitchen(hasKitchen);
        } else {
            throw new InstantiationException("Person with null-values not allowed!");
        }

        this.ID = getNextId();
    }

    public ArrayList<Person> getPossiblePartners(ArrayList<Person> persons) {
        ArrayList<Person> possiblePartners = new ArrayList<>();

        if (this.hasKitchen()) {
            return persons;
        } else {
            for (Person partner : persons) {
                if (partner.hasKitchen()) {
                    possiblePartners.add(partner);
                }
            }
        }

        return possiblePartners;
    }

    @Override
    public String toString() {
        return getLastName() + ", " + getPreName();
    }

    public String getListText() {
        return getPreName() + ", " + getLastName() + " (" + getAddress() + "; " + getTelephone() + "; notice: " + getNotice();
    }

    public boolean hasKitchen() {
        return hasKitchen;
    }

    public void setHasKitchen(boolean hasKitchen) {
        this.hasKitchen = hasKitchen;
    }

    public String getPreName() {
        return preName;
    }

    public void setPreName(String preName) {
        this.preName = preName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getNotice() {
        return notice;
    }

    public void setNotice(String notice) {
        this.notice = notice;
    }

    public Pair getIsPartOf() {
        return isPartOf;
    }

    public void setIsPartOf(Pair isPartOf) {
        this.isPartOf = isPartOf;
    }

    public int getID() {
        return ID;
    }

    @Override
    public int compareTo(Person p) {
        int cmpVal = this.lastName.toLowerCase().compareTo(p.getLastName().toLowerCase());
        if (cmpVal < 0) {
            return -1;
        } else if (cmpVal > 0) {
            return 1;
        } else {
            cmpVal = this.preName.toLowerCase().compareTo(p.getPreName().toLowerCase());
            if (cmpVal < 0) {
                return -1;
            } else if (cmpVal > 0) {
                return 1;
            } else {
                return 0;
            }
        }
    }
    
    private int getNextId() {
        startId++;
        return startId;
    }
}
