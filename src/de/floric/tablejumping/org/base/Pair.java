/* 
 * Copyright 2014 Florian Richter.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.floric.tablejumping.org.base;

import de.floric.tablejumping.util.Geocoding;

/**
 *
 * @author florian
 */
public class Pair implements Comparable<Pair> {

    private final int ID;
    private static int startId = 0;
    
    private boolean isHost;
    
    private Person pOne;
    private Person pTwo;
    
    private Meeting starter;
    private Meeting mainCourse;
    private Meeting dessert;
    
    private int useKitchenFrom = 0;

    public Pair(Person pOne, Person pTwo) throws InstantiationException {
        if (pOne != null && pTwo != null) {
            setpOne(pOne);
            setpTwo(pTwo);

            setDefaultKitchen();

        } else {
            throw new InstantiationException("Pair with unitializied person not possible!");
        }

        this.ID = getNextId();

        this.isHost = false;
    }

    private int getNextId() {
        startId++;
        return startId;
    }

    public Address getHostAddress() {

        boolean kitchenOneInvalid = useKitchenFrom == 1 && pOne.hasKitchen() == false;
        boolean kitchenTwoInvalid = useKitchenFrom == 2 && pTwo.hasKitchen() == false;
        
        if(kitchenOneInvalid) {
            if(pTwo.hasKitchen()) {
                useKitchenFrom = 2;
            }
        }
        if(kitchenTwoInvalid) {
            if(pOne.hasKitchen()) {
                useKitchenFrom = 1;
            }
        }
        if(pOne.hasKitchen() == false && pTwo.hasKitchen() == false) {
            useKitchenFrom = 0;
        }
         
        if (useKitchenFrom == 1) {
            return pOne.getAddress();
        } else if (useKitchenFrom == 2) {
            return pTwo.getAddress();
        } else {
            return null;
        }
    }

    public boolean isIsHost() {
        return isHost;
    }

    public void setIsHost(boolean isHost) {
        this.isHost = isHost;
    }

    public int getID() {
        return ID;
    }

    public Person getpOne() {
        return pOne;
    }

    public void setpOne(Person pOne) {
        setOldPersonRelation(this.pOne);
        
        this.pOne = pOne;
        pOne.setIsPartOf(this);
    }

    public Person getpTwo() {
        return pTwo;
    }

    public void setpTwo(Person pTwo) {
        setOldPersonRelation(this.pTwo);
        this.pTwo = pTwo;
        pTwo.setIsPartOf(this);
    }

    public void setStarter(Meeting starter) {
        this.starter = starter;
    }

    public void setMainCourse(Meeting mainCourse) {
        this.mainCourse = mainCourse;
    }

    public void setDessert(Meeting dessert) {
        this.dessert = dessert;
    }

    public Meeting getStarter() {
        return starter;
    }

    public Meeting getMainCourse() {
        return mainCourse;
    }

    public Meeting getDessert() {
        return dessert;
    }
    
    private void setOldPersonRelation(Person p) {
        if(p != null) {
            p.setIsPartOf(null);
        }
    }

    public int getUseKitchenFrom() {
        return useKitchenFrom;
    }

    public void setUseKitchenFrom(int useKitchenFrom) {
        if (useKitchenFrom == 1 || useKitchenFrom == 2) {
            this.useKitchenFrom = useKitchenFrom;
        } else {
            throw new IllegalArgumentException("Kitchen variable must be 1 or 2!");
        }
    }

    @Override
    public String toString() {
        StringBuilder strBld = new StringBuilder();

        strBld.append("Pair ").append(ID).append(": ");
        strBld.append(pOne);
        strBld.append(" and ");
        strBld.append(pTwo);

        return strBld.toString();
    }

    @Override
    public int compareTo(Pair p) {
        if (this.getID() > p.getID()) {
            return 1;
        } else if (this.getID() == p.getID()) {
            return 0;
        } else {
            return -1;
        }
    }

    private void setDefaultKitchen() {
        if (pOne.hasKitchen()) {
            setUseKitchenFrom(1);
        } else if (pTwo.hasKitchen()) {
            setUseKitchenFrom(2);
        } else {
            throw new IllegalArgumentException("No kitchen possible for pair " + this.ID);
        }
    }
    
    public void destroy() {
        pOne.setIsPartOf(null);
        pTwo.setIsPartOf(null);
    }
    
    public double getDistanceSum() {
        double sum = 0;
        
        Address startAdr = this.getStarter().getHost().getHostAddress();
        Address mainAdr = this.getMainCourse().getHost().getHostAddress();
        Address dessertAdr = this.getDessert().getHost().getHostAddress();

        sum += Geocoding.getDistance(startAdr.getLatitude(), startAdr.getLongitute(), mainAdr.getLatitude(), mainAdr.getLongitute());
        sum += Geocoding.getDistance(mainAdr.getLatitude(), mainAdr.getLongitute(), dessertAdr.getLatitude(), dessertAdr.getLongitute());
        
        return sum;
    }
}
