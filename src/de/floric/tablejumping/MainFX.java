/* 
 * Copyright 2014 Florian Richter.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.floric.tablejumping;

import de.floric.tablejumping.gui.controller.CalcViewController;
import de.floric.tablejumping.gui.controller.MainController;
import de.floric.tablejumping.gui.controller.PairViewController;
import de.floric.tablejumping.gui.controller.PersonViewController;
import de.floric.tablejumping.org.base.Pair;
import de.floric.tablejumping.org.base.Person;
import de.floric.tablejumping.util.Organization;
import de.floric.tablejumping.util.RandomData;
import java.io.IOException;
import java.net.URL;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author florian
 */
public class MainFX extends Application {

    private final Calendar cal = new GregorianCalendar(TimeZone.getDefault());
    private static final int MAX_RAND_LOCK_TIMES = 0;

    private MainController mainCtrl;
    private PersonViewController personCtrl;
    private PairViewController pairCtrl;
    private CalcViewController calcCtrl;
    private ObservableList<Person> participants;
    private ObservableList<Pair> pairs;
    
    private static MainFX appInstance;
    
    public static MainFX getAppInstance() {
        return appInstance;
    }

    public ObservableList<Person> getParticipants() {
        return participants;
    }

    public ObservableList<Pair> getPairs() {
        return pairs;
    }

    private Scene createScene() throws IOException {
        URL location = getClass().getResource("gui/mainWindow.fxml");

        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(location);
        fxmlLoader.setBuilderFactory(new JavaFXBuilderFactory());

        Parent root = (Parent) fxmlLoader.load(location.openStream());

        mainCtrl = fxmlLoader.getController();
        personCtrl = mainCtrl.getPersonView();
        pairCtrl = mainCtrl.getPairView();
        calcCtrl = mainCtrl.getCalcView();

        participants = personCtrl.getPersonListView().getItems();
        pairs = pairCtrl.getPairListView().getItems();

        pairCtrl.setPersonList(participants);
        calcCtrl.setPersonList(participants);
        calcCtrl.setPairList(pairs);

        return new Scene(root, 800, 600);
    }

    private void addRandomPairs(int pairsNr, int singlesNr, Date start, Date end) {

        int seed = 0;

        for (int i = 1; i <= pairsNr; i++) {
            Person pOne = RandomData.generateRandomPerson(seed);
            participants.add(pOne);
            seed++;

            Person pTwo = RandomData.generateRandomPerson(seed);
            if (pOne.hasKitchen() == false) {
                pTwo.setHasKitchen(true);
            }

            participants.add(pTwo);
            seed++;

            Pair p = null;
            try {
                p = new Pair(pOne, pTwo);
                pairs.add(p);
                pOne.setIsPartOf(p);
                pTwo.setIsPartOf(p);
            } catch (InstantiationException ex) {
                Logger.getLogger(MainFX.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        for (int i = 0; i < singlesNr; i++) {
            Person p = RandomData.generateRandomPerson(seed);
            participants.add(p);
            seed++;
        }
    }

    @Override
    public void start(Stage primaryStage) throws InstantiationException {

        System.out.println("Version: " + Organization.getVersion(this));

        try {
            Scene scene = createScene();
            
            appInstance = this;
            
            primaryStage.setTitle("TableJumping");
            primaryStage.setScene(scene);

            primaryStage.show();

        } catch (IOException ex) {
            Logger.getLogger(MainFX.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
