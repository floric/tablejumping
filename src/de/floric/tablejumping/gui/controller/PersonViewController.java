/* 
 * Copyright 2014 Florian Richter.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.floric.tablejumping.gui.controller;

import de.floric.tablejumping.gui.listener.PersonSelectionListener;
import de.floric.tablejumping.org.base.Address;
import de.floric.tablejumping.org.base.Pair;
import de.floric.tablejumping.org.base.Person;
import de.floric.tablejumping.util.EmailParser;
import de.floric.tablejumping.util.GuiUtil;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

/**
 *
 * @author florian
 */
public class PersonViewController implements Initializable {

    @FXML
    private ListView<Person> personList;

    @FXML
    private TextField persPreName;
    @FXML
    private TextField persLastName;
    @FXML
    private TextField persMail;
    @FXML
    private TextField persTelephone;
    @FXML
    private TextField persStreet;
    @FXML
    private TextField persZipCode;
    @FXML
    private TextField persCity;
    @FXML
    private TextField persAdrNotice;
    @FXML
    private TextArea persNotice;
    @FXML
    private CheckBox persHasKitchen;
    @FXML
    private Label partner;
    @FXML
    private Label kitchenAddr;

    @FXML
    private Button updButton;

    private Person selectedPerson = null;

    public ObservableList<Person> getPersonList() {
        return personList.getItems();
    }

    public boolean addPerson(Person p) {
        if (personList != null && p != null) {
            ObservableList<Person> data = personList.getItems();
            data.add(p);
            FXCollections.sort(data);
            return true;
        } else {
            return false;
        }
    }

    @FXML
    private void createPerson() {
        Person p = null;
        try {
            p = new Person("", "", "", "mail@mail.de", new Address("", "", ""), "", true);
            addPerson(p);
        } catch (InstantiationException ex) {
            Logger.getLogger(PersonViewController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    public void showPerson(Person p) {
        if (p != null) {
            selectedPerson = p;

            persPreName.setText(p.getPreName());
            persLastName.setText(p.getLastName());
            persTelephone.setText(p.getTelephone());
            persMail.setText(p.getMail());
            persStreet.setText(p.getAddress().getStreet());
            persZipCode.setText(String.valueOf(p.getAddress().getZipCode()));
            persCity.setText(p.getAddress().getCity());
            persNotice.setText(p.getNotice());

            if (p.hasKitchen()) {
                persHasKitchen.setSelected(true);
            } else {
                persHasKitchen.setSelected(false);
            }

            Pair pair = p.getIsPartOf();

            if (pair != null) {
                if (pair.getpOne().equals(p)) {
                    partner.setText(pair.getpTwo().getPreName() + " " + pair.getpTwo().getLastName());
                } else {
                    partner.setText(pair.getpOne().getPreName() + " " + pair.getpOne().getLastName());
                }
                Address hostAddr = pair.getHostAddress();
                if (hostAddr != null) {
                    kitchenAddr.setText(hostAddr.toString());
                } else {
                    kitchenAddr.setText("keine Küche verfügbar");
                }

            } else {
                partner.setText("-");
                kitchenAddr.setText("-");
            }
        }

    }

    @FXML
    private boolean removePerson() {
        if (selectedPerson != null) {

            Pair partOf = selectedPerson.getIsPartOf();

            if (partOf == null) {

                ObservableList<Person> list = personList.getItems();

                if (list.contains(selectedPerson)) {
                    list.remove(selectedPerson);
                    selectedPerson = null;
                    return true;
                } else {
                    return false;
                }
            } else {
                GuiUtil.showError("Person ist noch mit Paar " + partOf.getID() + " verknüpft!");
                return false;
            }
        } else {
            return false;
        }
    }

    public void updatePersonList() {
        ArrayList<Person> list = new ArrayList<>();
        list.addAll(personList.getItems());
        Collections.sort(list);

        personList.getItems().clear();

        for (Person p : list) {
            personList.getItems().add(p);
        }
    }

    @FXML
    private void filterByKitchen(ActionEvent e) {
        System.out.println("Filter by Kitchen");
    }

    @FXML
    private void filterByPartner(ActionEvent e) {
        System.out.println("Filter by Partner");
    }

    @FXML
    private void changePerson(ActionEvent e) {
        if (selectedPerson != null) {
            selectedPerson.setPreName(persPreName.getText());
            selectedPerson.setLastName(persLastName.getText());
            //TODO Parse telephone
            selectedPerson.setTelephone(persTelephone.getText());

            EmailParser emailParse = new EmailParser();

            if (emailParse.validate(persMail.getText())) {
                selectedPerson.setMail(persMail.getText());
            } else {
                //TODO Error
                System.out.println("Mail not valid");
            }

            Address oldAddr = selectedPerson.getAddress();
            if (oldAddr.getStreet().compareTo(persStreet.getText()) != 0 || oldAddr.getCity().compareTo(persCity.getText()) != 0 || oldAddr.getZipCode().compareTo(persZipCode.getText()) != 0) {
                try {
                    Address newAddr = new Address(persStreet.getText(), persCity.getText(), persZipCode.getText());
                    selectedPerson.setAddress(newAddr);

                } catch (NumberFormatException ex) {
                    System.out.println("ZIP code not valid!");
                }
            }

            selectedPerson.setNotice(persNotice.getText());
            selectedPerson.setHasKitchen(persHasKitchen.isSelected());

            updatePersonList();
        }
    }

    public ListView<Person> getPersonListView() {
        return personList;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        personList.getSelectionModel().selectedItemProperty().addListener(new PersonSelectionListener(this));
    }
}
