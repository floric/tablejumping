/* 
 * Copyright 2014 Florian Richter.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.floric.tablejumping.gui.controller;

import de.floric.tablejumping.MainFX;
import de.floric.tablejumping.org.base.Pair;
import de.floric.tablejumping.org.base.Person;
import de.floric.tablejumping.util.GuiUtil;
import de.floric.tablejumping.util.exchange.Exporter;
import java.io.File;
import java.util.ArrayList;
import javafx.fxml.FXML;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author florian
 */
public class MainController {

    @FXML
    AnchorPane personView;
    @FXML
    AnchorPane calcView;
    @FXML
    private PersonViewController personViewTabController;
    @FXML
    private CalcViewController calcViewTabController;
    @FXML
    private PairViewController pairViewTabController;

    @FXML
    private void updateUI() {
        if (calcViewTabController != null) {
            calcViewTabController.updateUi();
        }
    }

    @FXML
    private void showLicenses() {
        GuiUtil.showLicenses();
    }

    @FXML
    private void showInfo() {
        GuiUtil.showInfo();
    }

    @FXML
    private void importData() {
        final Stage newStage = new Stage();
        StageStyle style = StageStyle.UTILITY;
        newStage.initStyle(style);
        newStage.initModality(Modality.APPLICATION_MODAL);

        FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(
                new File(System.getProperty("user.home"))
        );
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("TableJumping Data", "*.tjda")
        );
        fileChooser.setTitle("Open a existing project");
        fileChooser.showOpenDialog(newStage);

    }

    @FXML
    private void exportData() {
        final Stage newStage = new Stage();
        StageStyle style = StageStyle.UTILITY;
        newStage.initStyle(style);
        newStage.initModality(Modality.APPLICATION_MODAL);

        FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(
                new File(System.getProperty("user.home"))
        );
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("TableJumping Data", "*.tjda")
        );
        fileChooser.setTitle("Export this project");
        File f = fileChooser.showSaveDialog(newStage);

        if (f != null) {
            ArrayList<Person> persons = new ArrayList<>();
            persons.addAll(MainFX.getAppInstance().getParticipants());

            ArrayList<Pair> pairs = new ArrayList<>();
            pairs.addAll(MainFX.getAppInstance().getPairs());

            Exporter exporter = new Exporter(persons, pairs);
            exporter.setExportPath(f.getAbsolutePath());
            exporter.exportData();
        } else {
            System.out.println("f is null");
        }

    }

    public PersonViewController getPersonView() {
        return personViewTabController;
    }

    public CalcViewController getCalcView() {
        return calcViewTabController;
    }

    public PairViewController getPairView() {
        return pairViewTabController;
    }
}
