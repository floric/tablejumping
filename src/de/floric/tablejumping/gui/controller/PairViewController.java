/* 
 * Copyright 2014 Florian Richter.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.floric.tablejumping.gui.controller;

import de.floric.tablejumping.gui.listener.PairSelectionListener;
import de.floric.tablejumping.org.base.Address;
import de.floric.tablejumping.org.base.Pair;
import de.floric.tablejumping.org.base.Person;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;

/**
 * FXML Controller class
 *
 * @author florian
 */
public class PairViewController implements Initializable {
    
    @FXML
    private ListView<Pair> pairList;
    @FXML
    private ChoiceBox<Person> pOne;
    @FXML
    private ChoiceBox<Person> pTwo;
    @FXML
    private ChoiceBox<Person> kitchenProvider;
    @FXML
    private Label street;
    @FXML
    private Label city;
    @FXML
    private Label notice;
    
    private ObservableList<Person> personList;
    private Pair selectedPair;
    
    public ListView<Pair> getPairListView() {
        return pairList;
    }
    
    @FXML
    public void showPair(Pair p) {
        if (p != null && pairList != null) {
            selectedPair = p;

            //updatePartnerChoices(p.getpOne(), p.getpTwo());
            pOne.setValue(p.getpOne());
            pTwo.setValue(p.getpTwo());
            
            updateKitchen();
        }
    }
    
    private void updatePartnerChoices(Person pOne, Person pTwo) {
        ArrayList<Person> possiblePartnersForOne = pOne.getPossiblePartners(new ArrayList<>(personList));
        ArrayList<Person> possiblePartnersForTwo = pTwo.getPossiblePartners(new ArrayList<>(personList));
        
        if (possiblePartnersForOne.size() == 0 || possiblePartnersForTwo.size() == 0) {
            System.out.println("no partners found");
        }
        
        ObservableList<Person> choicesForOne = FXCollections.observableArrayList();
        choicesForOne.addAll(possiblePartnersForOne);
        ObservableList<Person> choicesForTwo = FXCollections.observableArrayList();
        choicesForTwo.addAll(possiblePartnersForTwo);
        
        this.pOne.setItems(choicesForOne);
        this.pTwo.setItems(choicesForTwo);
    }
    
    @FXML
    private void changePartner(int nr, Person newPerson) {
        switch (nr) {
            case 1:
                selectedPair.setpOne(newPerson);
                break;
            case 2:
                selectedPair.setpTwo(newPerson);
                break;
            default:
                Logger.getLogger(PersonViewController.class.getName()).log(Level.SEVERE, "Unknown number of a partner: " + nr);
        }
        updatePairList();
        
        updateKitchen();
    }
    
    @FXML
    private void updateKitchen() {
        
        ObservableList<Person> persWithKitchen = FXCollections.observableArrayList();
        
        if (selectedPair.getpOne().hasKitchen()) {
            persWithKitchen.add(selectedPair.getpOne());
        }
        if (selectedPair.getpTwo().hasKitchen()) {
            persWithKitchen.add(selectedPair.getpTwo());
        }
        if (persWithKitchen.size() == 1) {
            kitchenProvider.setDisable(true);
        } else {
            kitchenProvider.setDisable(false);
        }
        
        kitchenProvider.setItems(persWithKitchen);
        if (persWithKitchen.isEmpty()) {
            kitchenProvider.setValue(null);
        } else {
            switch (selectedPair.getUseKitchenFrom()) {
                case 1:
                    kitchenProvider.setValue(selectedPair.getpOne());
                    break;
                case 2:
                    kitchenProvider.setValue(selectedPair.getpTwo());
                    break;
                default:
                    System.out.println("Kitchen value not valid: " + selectedPair.getUseKitchenFrom());
            }
        }
        showKitchenAddress();
    }
    
    private void showKitchenAddress() {
        if (kitchenProvider.getValue() != null) {
            Person provider = kitchenProvider.getValue();
            Address adr = provider.getAddress();
            street.setText(adr.getStreet());
            city.setText(adr.getZipCode() + " " + adr.getCity());
            notice.setText(adr.getNotice());
        } else {
            street.setText("-");
            city.setText("-");
            notice.setText("-");
        }
    }
    
    public boolean addPair(Pair p) {
        if (pairList != null && p != null) {
            ObservableList<Pair> data = pairList.getItems();
            data.add(p);
            FXCollections.sort(data);
            return true;
        } else {
            return false;
        }
    }
    
    @FXML
    private void createPair() {
        Pair p = null;
        try {
            p = new Pair(new Person("", "", "", "", new Address("", "", ""), "", true), new Person("", "", "", "", new Address("", "", ""), "", true));
            addPair(p);
        } catch (InstantiationException ex) {
            Logger.getLogger(PersonViewController.class.getName()).log(Level.SEVERE, "Failed creating the pair!", ex);
        }
    }
    
    public void setPersonList(ObservableList<Person> list) {
        this.personList = list;
        
        pOne.setItems(personList);
        pTwo.setItems(personList);
    }
    
    public void updatePairList() {
        ArrayList<Pair> list = new ArrayList<>();
        list.addAll(pairList.getItems());
        Collections.sort(list);
        
        pairList.getItems().clear();
        
        for (Pair p : list) {
            pairList.getItems().add(p);
        }
    }
    
    public void setPairList(ListView<Pair> pairList) {
        this.pairList = pairList;
    }
    
    @FXML
    private boolean removePair() {
        if (selectedPair != null) {
            
            ObservableList<Pair> list = pairList.getItems();
            
            if (list.contains(selectedPair)) {
                list.remove(selectedPair);
                selectedPair.destroy();
                selectedPair = null;
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        pairList.getSelectionModel().selectedItemProperty().addListener(new PairSelectionListener(this));
        
        pOne.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Person>() {
            
            @Override
            public void changed(ObservableValue<? extends Person> ov, Person oldPerson, Person newPerson) {
                if (newPerson != null) {
                    changePartner(1, newPerson);
                }
            }
            
        });
        
        pTwo.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Person>() {
            
            @Override
            public void changed(ObservableValue<? extends Person> ov, Person oldPerson, Person newPerson) {
                if (newPerson != null) {
                    changePartner(2, newPerson);
                }
            }
            
        });
        
        kitchenProvider.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Person>() {
            
            @Override
            public void changed(ObservableValue<? extends Person> ov, Person oldPerson, Person newPerson) {
                if (newPerson != null) {
                    
                    if (newPerson.equals(selectedPair.getpOne())) {
                        selectedPair.setUseKitchenFrom(1);
                        showKitchenAddress();
                        
                    } else if (newPerson.equals(selectedPair.getpTwo())) {
                        selectedPair.setUseKitchenFrom(2);
                        showKitchenAddress();
                        
                    } else {
                        Logger.getLogger(PersonViewController.class.getName()).log(Level.SEVERE, "Unknown person selected for the kitchen!");
                    }
                }
            }
        });
    }
    
}
