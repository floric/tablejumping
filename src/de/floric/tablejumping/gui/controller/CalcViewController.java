/* 
 * Copyright 2014 Florian Richter.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.floric.tablejumping.gui.controller;

import de.floric.tablejumping.gui.listener.ErrorSelectionListener;
import de.floric.tablejumping.org.base.Meeting;
import de.floric.tablejumping.org.base.Pair;
import de.floric.tablejumping.org.base.Person;
import de.floric.tablejumping.org.distribution.CalcAlgorithm;
import de.floric.tablejumping.org.distribution.IterativeAlgorithm;
import de.floric.tablejumping.org.validator.PairDependencyCheck;
import de.floric.tablejumping.org.validator.PairDetailCheck;
import de.floric.tablejumping.org.validator.PersonDetailCheck;
import de.floric.tablejumping.org.validator.Verifiable;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TitledPane;

/**
 *
 * @author florian
 */
public class CalcViewController implements Initializable {

    @FXML
    private ListView<Verifiable> testList;
    @FXML
    private Label description;
    @FXML
    private Label result;
    @FXML
    private Label meetings;
    @FXML
    private TitledPane calcTab;
    @FXML
    private TitledPane overviewTab;

    private ObservableList<Person> participants;
    private ObservableList<Pair> pairs;
    private Verifiable selectedTest;

    @FXML
    private void doCalculation() {
        //CalcAlgorithm alg = new EvolutionaryAlgorithm(new ArrayList<>(this.getPairs()), new ArrayList<>(this.getParticipants()));
        CalcAlgorithm alg = new IterativeAlgorithm(new ArrayList<>(this.getPairs()), new ArrayList<>(this.getParticipants()));
        
        alg.setInput(new ArrayList<>(pairs), new ArrayList<>(participants));
        alg.setImportanceValues(0, 1);
        alg.doCalculation();
        
        this.getCalcTab().setExpanded(true);
        this.getCalcTab().setDisable(false);
        this.getOverviewTab().setDisable(false);
        
        StringBuilder strBld = new StringBuilder();
        
        for(Meeting m: alg.getResults()) {
            strBld.append(m.getMeal() + " at " + m.getHost() + " with " + m.getGuestOne() + " and " + m.getGuestTwo() + "\n");
        }
        
        meetings.setText(strBld.toString());
        
    }
    
    public void setPersonList(ObservableList<Person> list) {
        this.participants = list;
    }

    public ObservableList<Person> getParticipants() {
        return participants;
    }

    public ObservableList<Pair> getPairs() {
        return pairs;
    }

    public void setPairList(ObservableList<Pair> list) {
        this.pairs = list;
    }

    public void setTestList(ListView<Verifiable> testList) {
        this.testList = testList;
    }

    public void setErrorDescription(String description) {
        this.description.setText(description);
    }

    public void setErrorResult(String result) {
        this.result.setText(result);
    }

    public Verifiable getSelectedTest() {
        return selectedTest;
    }

    public void setSelectedTest(Verifiable selectedTest) {
        this.selectedTest = selectedTest;
    }

    public TitledPane getCalcTab() {
        return calcTab;
    }

    public TitledPane getOverviewTab() {
        return overviewTab;
    }

    public void doTest(Verifiable test) {
        this.setErrorDescription(test.getDescription());

        this.setErrorResult("bitte warten...");

        test.setInput(new ArrayList<>(this.getPairs()), new ArrayList<>(this.getParticipants()));
        test.doCheck();

        if (test.isErrorFree()) {
            this.setErrorResult("Alles in Ordnung!");
        } else {
            this.setErrorResult(test.getErrorMessage());
        }

    }

    public void updateUi() {
        ArrayList<Verifiable> test = new ArrayList<>(testList.getItems());
        
        for(Verifiable t: test) {
            doTest(t); 
        }
        
        updateTitledPanes();
    }

    public void updateTitledPanes() {
        if (this.allTestsArePassed()) {
            this.getCalcTab().setDisable(false);
            this.getCalcTab().setExpanded(true);
        } else {
            this.getCalcTab().setExpanded(false);
            this.getCalcTab().setDisable(true);
            this.getOverviewTab().setDisable(true);
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        createTestSelectionList();

        testList.getSelectionModel().selectedItemProperty().addListener(new ErrorSelectionListener(this));
    }

    public boolean allTestsArePassed() {
        boolean isTrue = true;

        for (Verifiable test : testList.getItems()) {
            if (test.isErrorFree() == false) {
                isTrue = false;
                break;
            }
        }

        return isTrue;
    }

    private void createTestSelectionList() {
        PersonDetailCheck persDetCheck = new PersonDetailCheck();
        PairDependencyCheck pairDepCheck = new PairDependencyCheck();
        PairDetailCheck pairDetCheck = new PairDetailCheck();

        ObservableList<Verifiable> list = FXCollections.observableArrayList();

        list.add(persDetCheck);
        list.add(pairDepCheck);
        list.add(pairDetCheck);

        testList.setItems(list);
    }

}
