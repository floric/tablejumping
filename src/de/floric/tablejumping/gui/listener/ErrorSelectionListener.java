/* 
 * Copyright 2014 Florian Richter.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.floric.tablejumping.gui.listener;

import de.floric.tablejumping.gui.controller.CalcViewController;
import de.floric.tablejumping.org.base.Pair;
import de.floric.tablejumping.org.base.Person;
import de.floric.tablejumping.org.validator.Verifiable;
import java.util.ArrayList;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

/**
 *
 * @author florian
 */
public class ErrorSelectionListener implements ChangeListener<Verifiable> {

    private CalcViewController control = null;
    private ArrayList<Person> persons = new ArrayList<>();
    private ArrayList<Pair> pairs = new ArrayList<>();

    public ErrorSelectionListener(CalcViewController control) {
        this.control = control;
    }

    @Override
    public void changed(ObservableValue<? extends Verifiable> ov, Verifiable oldSelection, Verifiable newSelection) {
        control.setSelectedTest(newSelection);
        
        control.doTest(newSelection);
        
        //control.updateTitledPanes();
        
    }

}
