/* 
 * Copyright 2014 Florian Richter.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.floric.tablejumping.gui.listener;

import de.floric.tablejumping.gui.controller.PairViewController;
import de.floric.tablejumping.org.base.Pair;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

/**
 *
 * @author florian
 */
public class PairSelectionListener implements ChangeListener<Pair>{
    
    PairViewController control = null;

    public PairSelectionListener(PairViewController control) {
        this.control = control;
    }

    @Override
    public void changed(ObservableValue<? extends Pair> ov, Pair oldPair, Pair newPair) {
        control.showPair(newPair);
    }
    
}