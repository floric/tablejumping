/*
 * Copyright 2014 florian.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.floric.tablejumping.util;

import de.floric.tablejumping.MainFX;
import java.io.File;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author florian
 */
public class GuiUtil {
    
    private final static String SOURCE_LINK = "https://bitbucket.org/floric/tablejumping/";

    public static void showLicenses() {
        final Stage newStage = new Stage();
        StageStyle style = StageStyle.UTILITY;
        newStage.initStyle(style);
        newStage.initModality(Modality.WINDOW_MODAL);

        VBox comp = new VBox();
        Label info = new Label("Diese Software nutzt:");
        Label javaFX = new Label("JavaFX 2.0");
        Label quickJSON = new Label("quick-json\nApache License 2.0");
        Label nominatim = new Label("Nominatim der OSM\nDaten: Open Data Commons Open Database Lizenz (ODbL)");

        Button accept = new Button("OK");
        accept.setAlignment(Pos.CENTER);

        comp.getChildren().add(info);
        comp.getChildren().add(javaFX);
        comp.getChildren().add(quickJSON);
        comp.getChildren().add(nominatim);
        comp.getChildren().add(accept);

        Scene stageScene = new Scene(comp);

        // add style
        comp.getStyleClass().add("licensebox");

        File cssFile = new File(FileUtil.MAIN_PATH + "gui/css/style.css");
        stageScene.getStylesheets().add(cssFile.toURI().toString());

        newStage.setScene(stageScene);
        newStage.setResizable(false);

        accept.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                newStage.close();
            }
        });

        newStage.show();
    }
    
    public static void showInfo() {
        final Stage newStage = new Stage();
        newStage.initModality(Modality.APPLICATION_MODAL);

        VBox comp = new VBox();
        Label info = new Label("TableJumping\n\nEine Software zur Verwaltung von Running Dinners (der Begriff ist ein eingetragenes Warenzeichen)."
                + "\n\nVeröffentlicht unter der Apache Lizenz 2.0\n\n"
                + "Mehr Info unter:");
        Hyperlink link = new Hyperlink("https://bitbucket.org/floric/runningdinner/");

        Button accept = new Button("OK");
        accept.setAlignment(Pos.CENTER);

        comp.getChildren().add(info);
        comp.getChildren().add(link);
        comp.getChildren().add(accept);

        Scene stageScene = new Scene(comp);

        // add style
        comp.getStyleClass().add("licensebox");

        File cssFile = new File(FileUtil.MAIN_PATH + "gui/css/style.css");
        stageScene.getStylesheets().add(cssFile.toURI().toString());

        newStage.setScene(stageScene);
        newStage.setResizable(false);

        accept.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                newStage.close();
            }
        });

        link.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                Application app = MainFX.getAppInstance();
                
                app.getHostServices().showDocument(SOURCE_LINK);
            }
        });

        newStage.show();
    }

    public static void showError(String errormessage) {
        final Stage newStage = new Stage();
        newStage.initModality(Modality.APPLICATION_MODAL);

        VBox errorBox = new VBox();
        errorBox.setAlignment(Pos.CENTER);

        Label message = new Label(errormessage);
        message.setAlignment(Pos.CENTER);

        Button accept = new Button("OK");
        accept.setAlignment(Pos.CENTER);

        // add components
        errorBox.getChildren().add(message);
        errorBox.getChildren().add(accept);

        Scene stageScene = new Scene(errorBox);

        // add style
        errorBox.getStyleClass().add("errorbox");

        File cssFile = new File(FileUtil.MAIN_PATH + "gui/css/style.css");
        stageScene.getStylesheets().add(cssFile.toURI().toString());

        newStage.setScene(stageScene);
        newStage.setResizable(false);

        accept.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                newStage.close();
            }
        });

        newStage.show();
    }

}
