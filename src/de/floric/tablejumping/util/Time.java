/* 
 * Copyright 2014 Florian Richter.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.floric.tablejumping.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author florian
 */
abstract public class Time {

    public static long getDateInMillis(int year, int month, int date) {
        Calendar cal = new GregorianCalendar(year, month - 1, date);

        return cal.getTimeInMillis();
    }

    public static long getDayDifference(long millis) {
        return TimeUnit.MILLISECONDS.toDays(millis);
    }

    public static int getDay(Date d) {

        DateFormat df = new SimpleDateFormat("D");

        Calendar cal = new GregorianCalendar();
        cal.setTimeInMillis(d.getTime());

        return Integer.parseInt(df.format(cal.getTime()));
    }

    public static int getMonth(Date d) {

        DateFormat df = new SimpleDateFormat("M");

        Calendar cal = new GregorianCalendar();
        cal.setTimeInMillis(d.getTime());

        return Integer.parseInt(df.format(cal.getTime()));
    }

    public static int getYear(Date d) {

        DateFormat df = new SimpleDateFormat("yyyy");

        Calendar cal = new GregorianCalendar();
        cal.setTimeInMillis(d.getTime());

        return Integer.parseInt(df.format(cal.getTime()));
    }

    public static boolean isSameDay(Date dayOne, Date dayTwo) {
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal1.setTime(dayOne);
        cal2.setTime(dayTwo);
        boolean sameDay = cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR)
                && cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR);
        return sameDay;
    }

}
