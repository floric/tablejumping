/* 
 * Copyright 2014 Florian Richter.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.floric.tablejumping.util;

import com.json.exceptions.JSONParsingException;
import com.json.parsers.JSONParser;
import com.json.parsers.JsonParserFactory;
import de.floric.tablejumping.org.base.Address;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author florian
 */
public class Geocoding {

    final static String COUNTRY = "Germany";
    final static String ENCODING = "UTF-8";

    Address adr;
    double longitude = 0.0;
    double latitude = 0.0;

    public Geocoding(Address adr) {
        this.adr = adr;
    }

    public Address getAdr() {
        return adr;
    }

    public void setAdr(Address adr) {
        this.adr = adr;
    }

    public double getLongitute() {
        return longitude;
    }

    public void setLongitute(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public boolean fetchCoordinates() {
        boolean fetchSuccessfull = true;

        try {
            StringBuilder strBuild = new StringBuilder();

            strBuild.append("http://nominatim.openstreetmap.org/search?");
            strBuild.append("street=").append(URLEncoder.encode(adr.getStreet(), ENCODING));
            strBuild.append("&city=").append(URLEncoder.encode(adr.getCity(), ENCODING));
            strBuild.append("&country=").append(COUNTRY);
            strBuild.append("&format=json&limit=1&addressdetails=1");

            URL url = new URL(strBuild.toString());

            URLConnection connection = url.openConnection();

            JsonParserFactory factory = JsonParserFactory.getInstance();
            JSONParser parser = factory.newJsonParser();

            try {
                Map<String, ArrayList<HashMap<String, String>>> rootMap = parser.parseJson(connection.getInputStream(), ENCODING);
                ArrayList<HashMap<String, String>> results = (ArrayList<HashMap<String, String>>) rootMap.get("root");

                HashMap<String, String> values = results.get(0);

                latitude = Double.parseDouble(values.get("lat"));
                longitude = Double.parseDouble(values.get("lon"));
            } catch (JSONParsingException ex) {
                System.err.println("Address not found!");
            }

        } catch (MalformedURLException ex) {
            Logger.getLogger(Geocoding.class.getName()).log(Level.SEVERE, null, ex);
            fetchSuccessfull = false;
        } catch (IOException ex) {
            Logger.getLogger(Geocoding.class.getName()).log(Level.SEVERE, null, ex);
        }

        return fetchSuccessfull;
    }
    
    public static double getDistance(double x1, double y1, double x2, double y2) {
        return 6378.388 * Math.acos(Math.sin(Math.toRadians(x1)) * Math.sin(Math.toRadians(x2)) + Math.cos(Math.toRadians(x1)) * Math.cos(Math.toRadians(x2)) * Math.cos(Math.toRadians(y2 - y1)));
    }

}
