/* 
 * Copyright 2014 Florian Richter.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.floric.tablejumping.util;

import de.floric.tablejumping.org.base.Address;
import de.floric.tablejumping.org.base.Person;
import static de.floric.tablejumping.util.Time.getDayDifference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author florian
 */
abstract public class RandomData {
    
    

    public static Person generateRandomPerson(long seed) {

        Random rnd = new Random(seed);
        String preName = "P" + rnd.nextInt(9999);
        Address address = new Address("Randstreet " + rnd.nextInt(99), "RandomCity", "12345");
        Person p = null;
        try {
            p = new Person(preName, "Random", ("0" + rnd.nextInt(18000000)), "mail@provider.de", address, "bla", rnd.nextBoolean());
        } catch (InstantiationException ex) {
            Logger.getLogger(RandomData.class.getName()).log(Level.SEVERE, null, ex);
        }

        return p;
    }

    public static ArrayList<Date> generatedRandomBlockList(Date start, Date end, int maxLockTimes, long seed) throws IllegalArgumentException {
        ArrayList<Date> blockedDates = new ArrayList<>();

        long timeDifference = end.getTime() - start.getTime();

        if (timeDifference < 0) {
            throw new IllegalArgumentException("Start date after end date!");
        }

        long days = getDayDifference(timeDifference);

        Random rnd = new Random();
        int iterations = rnd.nextInt(maxLockTimes + 1);

        for (int i = 0; i < iterations; i++) {

            Date d = new Date(start.getTime() + (24 * 60 * 60 * 1000 * rnd.nextInt((int) days + 1)));

            boolean notAddedYet = true;

            for (Date knownDate : blockedDates) {
                if (Time.isSameDay(knownDate, d) == false) {
                    notAddedYet = true;
                } else {
                    notAddedYet = false;
                    break;
                }
            }
            
            if(notAddedYet) {
                blockedDates.add(d);
            }

        }

        Collections.sort(blockedDates);

        return blockedDates;
    }

}
