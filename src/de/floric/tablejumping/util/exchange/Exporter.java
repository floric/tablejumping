/* 
 * Copyright 2014 Florian Richter.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.floric.tablejumping.util.exchange;

import de.floric.tablejumping.org.base.Address;
import de.floric.tablejumping.org.base.Pair;
import de.floric.tablejumping.org.base.Person;
import java.io.File;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author florian
 */
public final class Exporter {

    private ArrayList<Person> persons;
    private ArrayList<Pair> pairs;
    private String exportPath = System.getProperty("user.home");

    public Exporter(ArrayList<Person> persons, ArrayList<Pair> pairs) {
        setPairs(pairs);
        setPersons(persons);
    }

    public void setPersons(ArrayList<Person> persons) {
        if (persons != null) {
            this.persons = persons;
        } else {
            this.persons = new ArrayList<>();
        }
    }

    public void setPairs(ArrayList<Pair> pairs) {
        if (pairs != null) {
            this.pairs = pairs;
        } else {
            this.pairs = new ArrayList<>();
        }
    }

    public String getExportPath() {
        return exportPath;
    }

    public void setExportPath(String exportPath) {
        this.exportPath = exportPath;
    }

    public boolean exportData() {
        Document doc = createDocument();

        if (doc != null) {
            try {
                TransformerFactory transformerFactory = TransformerFactory.newInstance();
                Transformer transformer = transformerFactory.newTransformer();

                DOMSource source = new DOMSource(doc);
                StreamResult result = new StreamResult(new File(exportPath));

                transformer.transform(source, result);
            } catch (TransformerConfigurationException ex) {
                Logger.getLogger(Exporter.class.getName()).log(Level.SEVERE, null, ex);
            } catch (TransformerException ex) {
                Logger.getLogger(Exporter.class.getName()).log(Level.SEVERE, null, ex);
            }

            return true;
        } else {

            return false;
        }
    }

    private Element createPersonNode(Person p, Document doc) {
        Element personNode = doc.createElement("person");
        personNode.setAttribute("id", String.valueOf(p.getID()));
        
        Element preName = doc.createElement("prename");
        preName.appendChild(doc.createTextNode(p.getPreName()));
        personNode.appendChild(preName);

        Element lastname = doc.createElement("lastname");
        lastname.appendChild(doc.createTextNode(p.getLastName()));
        personNode.appendChild(lastname);

        Element mail = doc.createElement("mail");
        mail.appendChild(doc.createTextNode(p.getMail()));
        personNode.appendChild(mail);
        
        Element telephone = doc.createElement("telephone");
        telephone.appendChild(doc.createTextNode(p.getTelephone()));
        personNode.appendChild(telephone);

        Address addr = p.getAddress();
        Element address = doc.createElement("address");
        personNode.appendChild(address);

        Element street = doc.createElement("street");
        street.appendChild(doc.createTextNode(addr.getStreet()));
        address.appendChild(street);
        Element zipCode = doc.createElement("zip");
        zipCode.appendChild(doc.createTextNode(addr.getZipCode()));
        address.appendChild(zipCode);
        Element city = doc.createElement("city");
        city.appendChild(doc.createTextNode(addr.getCity()));
        address.appendChild(city);
        Element coords = doc.createElement("coords");
        address.appendChild(coords);
        coords.setAttribute("long", String.valueOf(addr.getLongitute()));
        coords.setAttribute("lat", String.valueOf(addr.getLatitude()));

        Element notice = doc.createElement("notice");
        notice.appendChild(doc.createTextNode(p.getNotice()));
        personNode.appendChild(notice);

        Element hasKitchen = doc.createElement("kitchen");
        hasKitchen.appendChild(doc.createTextNode(String.valueOf(p.hasKitchen())));
        personNode.appendChild(hasKitchen);
        
        return personNode;
    }
    
    private Element createPairNode(Pair p, Document doc) {
        Element pairNode = doc.createElement("pair");
        pairNode.setAttribute("id", String.valueOf(p.getID()));
        
        Element personElem = doc.createElement("persons");
        personElem.setAttribute("first", String.valueOf(p.getpOne().getID()));
        personElem.setAttribute("second", String.valueOf(p.getpTwo().getID()));
        pairNode.appendChild(personElem);
        
        Element host = doc.createElement("host");
        host.appendChild(doc.createTextNode(String.valueOf(p.getUseKitchenFrom())));
        pairNode.appendChild(host);

        return pairNode;
    }

    private Document createDocument() {
        Document doc = null;

        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            doc = docBuilder.newDocument();
            Element rootElement = doc.createElement("project");
            doc.appendChild(rootElement);

            // add persons
            Element personList = doc.createElement("personlist");
            rootElement.appendChild(personList);

            for (Person p : persons) {
                Element personNode = createPersonNode(p, doc);
                
                personList.appendChild(personNode);
            }
            
            // add pairs
            Element pairsList = doc.createElement("pairslist");
            rootElement.appendChild(pairsList);

            for (Pair p : pairs) {
                Element pairNode = createPairNode(p, doc);
                
                pairsList.appendChild(pairNode);
            }

            return doc;
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(Exporter.class.getName()).log(Level.SEVERE, null, ex);
        }

        return doc;
    }

}
